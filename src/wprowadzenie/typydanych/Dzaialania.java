package wprowadzenie.typydanych;

public class Dzaialania {
    public static void main(String[] args) {
        System.out.println(2 + 3);
        System.out.println(2 - 4);
        System.out.println(5 / 2);
        System.out.println(5.0 / 2);
        System.out.println(5 / 2.0);
        System.out.println(5.0 / 2.0);
        System.out.println(100L - 10);
        System.out.println(2f - 3);
        System.out.println(5f / 2);
        System.out.println(5d / 2);

        System.out.println(false == false);
        System.out.println(false != true);
        System.out.println(!true);
        System.out.println(2 > 4);
        System.out.println(3 > 5);
        System.out.println((3 == 3) && (3 == 4));
        System.out.println((3 != 5) || (3 == 5));
        System.out.println((2 + 4) > (1 + 3));
        System.out.println("cos".equals("cos"));
        System.out.println("cos" == "cos");

    }
}
