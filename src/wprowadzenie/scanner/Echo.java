package wprowadzenie.scanner;

import java.util.Scanner;

public class Echo {
    public static void main(String[] args) {

        System.out.println("Cześć, jak Tobie na imię?");
        Scanner s = new Scanner(System.in);
        String imie = s.next();
        String nazwisko = s.next();

        System.out.println("Witam," + imie + " " + nazwisko);

    }
}
