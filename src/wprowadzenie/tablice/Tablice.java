package wprowadzenie.tablice;

public class Tablice {
    public static void main(String[] args) {

        int [] tablica = new int[] { 1, 3, 5, 10 };

        System.out.println(tablica[0]);
        System.out.println(tablica[1]);

        for (int i = 0; i < 4; i++) {
            System.out.println(tablica[i]);
        }

        for (int i = 0; i < 4; i++) {

            if ((i % 2) == 0 )
            {
                System.out.println(tablica[i]);
            }
        }

        for (int i = 0; i < 4; i++) {

            if ((tablica[i] % 2 ) == 0 )
            {
                System.out.println(tablica[i]);
            }
        }

        for (int i = (tablica.length) - 1; i >= 0  ; i--) {

            System.out.println(tablica[i]);
        }
    }
}
