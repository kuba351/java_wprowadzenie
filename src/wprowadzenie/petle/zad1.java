package wprowadzenie.petle;

public class zad1 {
    public static void main(String[] args) {

        for (int i = 1; i <= 100; i++)
        {
            System.out.println(i);
        }

        //////////////////////////////////////

        for (int i = 1000; i < 1020; i++) {
            System.out.print(i + ", ");
        }

        System.out.println();
        ///////////////////////////////////////

        for (int i = -30; i < 1001; i++) {

            if ((i % 5) == 0)
            {
                System.out.println(i);
            }
        }

        ////////////////////////////////////////

        for (int i = 0; i < 101; i++) {

            if ((i % 3) == 0)
            {
                System.out.println(i);
            }
        }

        /////////////////////////////////////////

        for (int i = 30; i < 301; i++) {

            if (((i % 3) == 0) && ((i % 5) == 0))
            {
                System.out.println(i);
            }
        }

        /////////////////////////////////////////

        for (int i = -300; i <= 300; i++) {

            if ((i % 2) != 0)
            {
                System.out.print(i + ";");
            }
        }

        System.out.println();

        ////////////////////////////////////////////

        for (int i = -100; i <= 100; i++) {

            if ((i % 2) == 0)
            {
                System.out.print(i + ";");
            }
        }

        System.out.println();

        //////////////////////////////////////////////

        for (int i = 97; i <= 122; i++) {

        char c = (char)i;
        System.out.println(c);

        }

        ///////////////////////////////////////////////

        for (int i = 65; i <= 90; i++) {

            char c = (char)i;
            System.out.println(c);

        }

        ///////////////////////////////////////////////

        for (int i = 65; i <= 90; i += 2) {

            char c = (char)i;
            System.out.println(c);
        }

        ///////////////////////////////////////////////

        for (int i = 98; i <= 122; i += 2) {

            if ((i % 5) == 0) {
                char c = (char) i;
                System.out.println(c);
            }
        }

        ///////////////////////////////////////////////

        String str = "Hello World";

        for (int i = 1; i < 101; i++) {

                System.out.println(i + ". Hello World");
        }
    }
}
