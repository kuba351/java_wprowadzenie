package wprowadzenie.petle;

public class zad1while {

    public static void main(String[] args) {

        int i = 1;
        while (i < 101)
        {
            System.out.println(i);
            i++;
        }

        ///////////////////////////////////////////////////

        i = 1000;
        while (i < 1021)
        {
            System.out.print(i + ", ");
            i++;
        }

        System.out.println();

        //////////////////////////////////////////////////////

        i = -30;
        while (i < 1001)
        {
            if ((i % 5) == 0) {
                System.out.println(i);
            }
            i++;
        }

        ///////////////////////////////////////////////////

        int iterator = 30;

        while (iterator <= 300)
        {
            i++;
        }
    }
}
