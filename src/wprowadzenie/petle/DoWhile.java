package wprowadzenie.petle;

import java.util.Random;
import java.util.Scanner;

public class DoWhile {
    public static void main(String[] args) {

        Scanner s = new Scanner(System.in);
        Random r = new Random();
        int szukana = r.nextInt(100);
        int liczba;

        do {
            System.out.println("Podaj liczbę:");
            liczba = s.nextInt();

            if (liczba == szukana)
            {
                System.out.println("Trafileś!");
            } else if (liczba > szukana)
            {
                System.out.println("za duża!");
            }else if (liczba < szukana)
            {
                System.out.println("za mała!");
            }
        } while (liczba != szukana);
    }
}
